from django.urls import path
from projects.views import list_projects, show_project
from django.urls import include

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("accounts/", include("accounts.urls")),
    path("<int:id>", show_project, name="show_project"),
]
