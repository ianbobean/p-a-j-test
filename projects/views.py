from projects.models import Project
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request):
    # use model to get all instances of Project
    # assign that to a variable
    projects = Project.objects.filter(owner=request.user)

    # use that in context dict
    context = {"projects": projects}
    # pass that to a template
    # render that template
    return render(request, "projects/index.html", context)


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )
