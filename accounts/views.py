from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from projects.models import Project
from .forms import SignUpForm


@login_required
def dashboard_view(request):
    user_projects = Project.objects.filter(owners=request.user)
    return render(request, "dashboard.html", {"projects": user_projects})


@login_required
def some_other_view(request):
    return redirect("dashboard")


def user_login(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")

            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("dashboard")

            error_message = "Invalid username or password."
            return render(
                request,
                "accounts/login.html",
                {"form": form, "error_message": error_message},
            )

    else:
        form = AuthenticationForm()

    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("dashboard")
    else:
        form = SignUpForm()

    context = {"form": form}
    return render(request, "accounts/signup.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")
